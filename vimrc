set nocompatible
set backspace=2

filetype off

call plug#begin('~/.vim/plugged')

" Edition
Plug 'godlygeek/tabular'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-surround'

" Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Integrations
Plug 'scrooloose/syntastic'
Plug 'tpope/vim-fugitive'

" UI
Plug 'ervandew/supertab'
Plug 'vim-airline/vim-airline'
Plug 'wincent/command-t', {
    \ 'do': 'cd ruby/command-t/ext/command-t && ruby extconf.rb && make' }
Plug 'majutsushi/tagbar'

" Languages
Plug 'hdima/python-syntax'
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'vim-jp/cpp-vim'
" Plug 'davidhalter/jedi-vim'
Plug 'mitsuhiko/vim-jinja'
Plug 'seirl/vim-jinja-languages'
Plug 'lervag/vimtex'

call plug#end()


filetype plugin indent on "filetype-based indentation detection
syntax on

"Display extra whitespaces in green
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=green guibg=red
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
if version >= 702
    autocmd BufWinLeave * call clearmatches()
endif


"Display
set scrolloff=5 "lines offset before starting scrolling
set number "show the number of lines
set laststatus=2 "always display the status line
set wildmenu
set wildmode=longest,list
if version >= 703
  set colorcolumn=+1
endif
if version >= 700
  set relativenumber
endif
colorscheme delroth-molokai
set t_Co=256

set list
set listchars=tab:→\ ,nbsp:¤
highlight SpecialKey ctermfg=59

"Edit
set expandtab "spaces instead of tabs
set smarttab "insert shiftwidth in the beginning of line or sts.
set tabstop=4
set softtabstop=4 "length of a fake tab in expandtab mode
set shiftwidth=4 "length of indentation
set autoindent "duh
set textwidth=79
set wrap

"Search
set nohlsearch "remove the persistant highlight after searches
set incsearch "search incrementally

"Directories
set backup
set backupdir=~/.vim/backup
set directory=~/.vim/directory
silent !mkdir -p ~/.vim/backup
silent !mkdir -p ~/.vim/directory
if version >= 703
    set undofile
    set undodir=~/.vim/undo
    silent !mkdir -p ~/.vim/undo
endif

"Other
set noerrorbells "remove terminal bells on error
set modelines=0 "modelines are the spawn of satan
set timeout timeoutlen=5000 ttimeoutlen=100 "Fix <ESC>+O timeout

"Keys & mappings
let mapleader=" "
set pastetoggle=<F2>
map <c-y> 0df:dwi* <Esc>A: Here.<Esc><CR>0
vmap <c-y> :normal <c-y><CR>
map Q <Nop>

"Abbreviations
cabbr <expr> %% expand('%:h')

command Mk silent make | unsilent redraw!

"Indent
function! GnuIndent()
    setlocal cinoptions=>4,f0,n-2,{2,^-2,:2,=2,g0,h2,p5,t0,+0,(0,u0,w1,m1
    setlocal shiftwidth=2
endfunction

"Plugins
let g:airline_powerline_fonts = 0
let g:airline_symbols = {'space': ' ', 'paste': 'PASTE', 'maxlinenr': '', 'notexists': 'Ɇ', 'readonly': '⊝', 'spell': 'SPELL', 'modified': '+', 'linenr': '≡ ', 'crypt': '🔒', 'branch': '꜓', 'whitespace': '=.'}

let g:UltiSnipsSnippetDirectories = ["bundle/vim-snippets/UltiSnips", "ultisnippets"]

let g:syntastic_cpp_check_header = 1
let g:syntastic_python_python_exec = 'python3'
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_quiet_messages = {'regex': '.*#pragma once in main file.*'}
let g:syntastic_rst_checkers=[] " 'sphinx']

let g:CommandTHighlightColor = 'Pmenu'
let g:CommandTCancelMap = ['<C-c>']
let g:CommandTTraverseSCM = 'pwd'

let g:pymode_python = 'python3'
let g:pymode_lint_on_write = 0
let g:pymode_folding = 0
let g:pymode_rope_lookup_project = 0
let g:pymode_rope = 0
let g:pymode_rope_complete_on_dot = 0

let g:tex_flavor = 'latex'
