#!/bin/sh

SCRIPT=$(readlink -f $0)
P=`dirname $SCRIPT`

sedescape() {
    echo $1 | sed -e 's/[\/&]/\\&/g'
}

linstall() {
    echo Linking ${2#.}...
    if [ -L $1/$2 ]; then
        rm $1/$2;
    fi
    ln -sT $P/${2#.} $1/$2
}

cinstall() {
    echo Copying ${2#.}...
    if [ -L $1/$2 ]; then
        rm $1/$2;
    fi
    cp $P/${2#.} $1/$2
}

paramask() {
    if [ ! -f $1 ]; then
        echo Non-existant file $1
    else
        echo Customize $1...
        FILE=$1
        shift 1
        for i in $*; do
            echo -n $'\t'$i': '
            read val
            val=`sedescape $val`
            sed -i 's/@\[\['$i'\]\]/'$val'/' $FILE
        done
    fi
}

paramset() {
    if [ ! -f $1 ]; then
        echo Non-existant or non-regular file $1
    else
        val=`sedescape $3`
        sed -i 's/@\[\['$2'\]\]/'$val'/' $1
    fi
}

linstall ~/.config git
linstall ~/.config i3
linstall ~/.config hg
linstall ~/.config ncmpcpp
linstall ~/.config picom

linstall ~ .screenrc
linstall ~ .vimrc
linstall ~ .vim
linstall ~ .XCompose
linstall ~ .zsh
linstall ~ .zshrc
linstall ~ .urxvt
linstall ~ .tmux.conf
linstall ~ .Xdefaults
linstall ~ .ssh/config
linstall ~ .ssh/authorized_keys

./rm_old_symlinks.sh
