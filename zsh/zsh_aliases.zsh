xsu() { su -c "$*"; }
sizes () { p=${1:-.}; du -sh $p/*(N) $p/.*(N) | sort -h }

alias rscp="rsync --rsh='ssh' -tz --info=progress2"
alias irc='ssh -t seirl@koin.fr tmux attach -dt irc'
alias mirc='mosh -- seirl@koin.fr tmux attach -dt irc'
alias ip='ip -c'

upload() {
    rscp -r $* seirl@koin.fr:www/koin.fr/upload
    for i in $*; do
        echo "http://koin.fr/upload/$( basename $i )"
    done
}

pub() {
    rscp -r $* seirl@koin.fr:www/koin.fr/pub
    for i in $*; do
        echo "http://koin.fr/pub/$( basename $i )"
    done
}

koin() {
    echo ' __              __    KOIN'
    echo ' \ \       ___  / /      KOIN'
    echo '  \ \     / _ \/ /   KOIN   KOIN'
    echo '   \ \   | (0) \ \       KOIN'
    echo '    \_\___\___/ \_\    KOIN'
    echo '     |_____|'

    imgdir="$( xdg-user-dir PICTURES 2>/dev/null || echo ~/Pictures )"
    shotdir="$imgdir/shots"
    rand=$( head -n256 /dev/urandom | base64 | tr -d '/+' | head -c8 )
    shotpath="$shotdir/koin-$(date +%Y-%m-%d-%H%M%S)-$rand.png"
    mkdir -p "$shotdir"
    import $( [ "$1" = "-a" ] && echo -window root ) "$shotpath"
    upload "$shotpath"
}

alias xlock='xlock -bg black -fg white -font fixed'

function 4chandl () {
    wget -e robots=off -N -nvdp -t 0 -Hrkl 0 -nc -I \*/src/ -P . "$1"
}

eb() {
  $*
  print '\a'
}

function o () { xdg-open $* >/dev/null 2>/dev/null &! }
