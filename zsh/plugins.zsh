sourceif() {
    if [ -f "$1" ]; then
        source "$1"
    fi
}

sourceif ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
sourceif /usr/share/autojump/autojump.zsh

if command -v direnv >/dev/null; then
    eval "$(direnv hook zsh)"
fi
