# Host completion
h=()
if [[ -r ~/.ssh/config ]]; then
      h=($h ${${${(@M)${(f)"$(cat ~/.ssh/config)"}:#Host *}#Host }:#*[*?]*})
  fi
  if [[ -r ~/.ssh/known_hosts ]]; then
        h=($h ${${${(f)"$(cat ~/.ssh/known_hosts{,2} || true)"}%%\ *}%%,*}) 2>/dev/null
    fi
    if [[ $#h -gt 0 ]]; then
          zstyle ':completion:*:ssh:*' hosts $h
            zstyle ':completion:*:slogin:*' hosts $h
        fi

# remote completion
zstyle ':completion:*:scp:*' tag-order \
    'hosts:-host hosts:-domain:domain hosts:-ipaddr:IP\ address *'
zstyle ':completion:*:scp:*' group-order \
    users files all-files hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:ssh:*' tag-order \
    users 'hosts:-host hosts:-domain:domain hosts:-ipaddr:IP\ address *'
zstyle ':completion:*:ssh:*' group-order \
    hosts-domain hosts-host users hosts-ipaddr

zstyle ':completion:*:(ssh|scp|rsync):*:hosts-host' ignored-patterns \
    '*.*' loopback localhost
zstyle ':completion:*:(ssh|scp|rsync):*:hosts-domain' ignored-patterns \
    '<->.<->.<->.<->' '^*.*' '*@*'
zstyle ':completion:*:(ssh|scp|rsync):*:hosts-ipaddr' ignored-patterns \
    '^<->.<->.<->.<->' '127.0.0.<->'
zstyle ':completion:*:(ssh|scp|rsync):*:users' ignored-patterns \
    adm bin daemon halt lp named shutdown sync
