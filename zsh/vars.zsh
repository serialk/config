export FULLNAME="Antoine Pietri"
export EMAIL="antoine.pietri1@gmail.com"
export XDG_CONFIG_HOME="$HOME/.config"

export PATH="$PATH:$HOME/bin"

if [ -z "$SSH_AUTH_SOCK" ]; then
    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
fi
