#!/bin/bash


rm_old() {
    if [ -L "$1" ]; then
        rm "$1";
    elif [ -e "$1" ]; then
        echo "error: cannot remove $1: not a symlink"
    fi
}

rm_old ~/.gitconfig
rm_old ~/.gitignore
rm_old ~/.hgrc
rm_old ~/.ncmpcpp
